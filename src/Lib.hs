{-# LANGUAGE AllowAmbiguousTypes, DataKinds, DeriveAnyClass, DeriveGeneric
  , FlexibleContexts, FlexibleInstances, MultiParamTypeClasses, NoTypeInType
  , PolyKinds, RankNTypes, ScopedTypeVariables, TypeApplications, TypeFamilies
  , TypeOperators, UndecidableInstances #-}
{-# OPTIONS_GHC -fplugin GHC.TypeLits.Symbols.Solver -fconstraint-solver-iterations=0 #-}

module Lib where

import Data.Functor.Identity
import Data.Promotion.Prelude.List hiding (Tail)
import Generics.SOP.Record
import Generics.SOP.Sing (SListI)
import Generics.SOP.Type.Metadata
import Generics.SOP.Universe
import qualified GHC.Generics as GHC
import GHC.TypeLits (KnownSymbol, Symbol)
import GHC.TypeLits.Symbols
import Squeal.PostgreSQL
  ( (:::), ColumnConstraint (..), ColumnsType, NullityType(..), PGType (..), SchemumType (..) )
import qualified Squeal.PostgreSQL as SQL

data K x
data T x i

type family C f x i where
    C K x i = T x i
    C _ x _ = x

type family SchemaOf' f where
    SchemaOf' '[]      = '[]
    SchemaOf' (h ': t) = SP h ': SchemaOf' t

type family SP x where
    SP '(l, T _ i) = l ::: 'NoDef SQL.:=> 'NotNull i

type family (:=>) ft i where
    (:=>) (f t) i = C f t i

type DataOf a   = a Identity
type SchemaOf a = SchemaOf' (RecordCodeOf (a K))

type family DTName (a :: DatatypeInfo) :: Symbol where
    DTName ('ADT _ n _)     = n
    DTName ('Newtype _ n _) = n

-- type TableOf a = DTName (DatatypeInfoOf (a Identity)) ::: 'Table ('[] SQL.:=> SchemaOf a)

type family DName (a :: *) :: Symbol where
    DName a = DTName (DatatypeInfoOf a)

type family FromCharList (cs :: [Symbol]) where
    FromCharList '[]       = ""
    FromCharList (c ': cs) = c +++ FromCharList cs

type family Lower' (x :: Symbol) where
    Lower' "A" = "_a"
    Lower' "R" = "_r"
    Lower' x   = x

type family Lower (x :: [Symbol]) where
    Lower '[] = '[]
    Lower (c ': cs) = Lower' c ': Lower cs

-- TODO: base on data constructor name instead?
type TableCase a = Tail (FromCharList (Init (Lower (ToCharList a))))

-- If a isn't constrained to *, we get weird kind mis-match errors
class Tbl (a :: *) where
    type TblName a :: Symbol
    type TblName a = TableCase (DName a)
    -- Cannot promote type alias ColumnsType
    type TblSchema a :: [(Symbol, (ColumnConstraint, NullityType))]
    type TblSchema a = SchemaOf' (RecordCodeOf a)

type TableDef a = TblName a ::: 'Table ('[] SQL.:=> TblSchema a)

type family TablesDef l where
    TablesDef '[]      = '[]
    TablesDef (h ': t) = TableDef h ': TablesDef t

class (SQL.PGTyped sch (SQL.PGTypeOf t)) => PGNullTyped sch (t :: SQL.NullityType) where
    pgnulltype :: SQL.ColumnTypeExpression sch ('SQL.NoDef SQL.:=> t)

instance (SQL.PGTyped sch t) => PGNullTyped sch (SQL.Null t) where
    pgnulltype = SQL.nullable SQL.pgtype

instance (SQL.PGTyped sch t) => PGNullTyped sch (SQL.NotNull t) where
    pgnulltype = SQL.notNullable SQL.pgtype

row :: forall (l :: Symbol) (t :: SQL.NullityType).
       (KnownSymbol l, PGNullTyped '[] t)
    => SQL.Aliased (SQL.ColumnTypeExpression '[]) '(l, '( 'SQL.NoDef, t))
row = (pgnulltype @'[] @t) `SQL.As` (SQL.Alias @l)

class Row x where
    rowX :: SQL.NP (SQL.Aliased (SQL.ColumnTypeExpression '[])) x

instance Row '[] where
    rowX = SQL.Nil

instance (KnownSymbol l, Row t, PGNullTyped '[] ty) => Row ('(l, 'SQL.NoDef SQL.:=> ty) ': t) where
    rowX = (row @l @ty) SQL.:* (rowX @t)

createTable :: forall a c0 c1.
            ( KnownSymbol (TblName a)
            , Row (TblSchema a) --, Row c1
            , TblSchema a ~ (c0 ': c1), SListI c1 )
            => SQL.Definition '[] '[TableDef a]
createTable = SQL.createTable (SQL.Alias @(TblName a)) (rowX @(TblSchema a)) SQL.Nil

---

data Rofl' f = Rofl { f1 :: f Int :=> 'PGint4 }
             deriving (GHC.Generic, Generic, HasDatatypeInfo, Tbl)

type Rofl = DataOf Rofl'

test :: SQL.Definition '[] '[TableDef (Rofl' K)]
test = createTable @(Rofl' K)
